<?php


namespace App\Controller;


use App\Entity\Booking;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\Tenant;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Enum\BookingObjectEnum;
use App\Repository\BookingObjectRepository;
use App\Repository\BookingRepository;
use App\Repository\ClientRepository;
use App\Repository\CottageRepository;
use App\Repository\LandlordRepository;
use App\Repository\PensionRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BookingObjectController
 * @package App\Controller
 * @Route("/object")
 */
class BookingObjectController extends Controller
{
    /**
     * @Route("/create", name="create_new_object")
     * @param Request $request
     * @param LandlordRepository $landlordRepository
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     */
    public function createNewObject(
        Request $request,
        ClientRepository $landlordRepository,
        EntityManagerInterface $manager,
        BookingObjectRepository $bookingObjectRepository
    )
    {
        $data = $request->request->all();
        try {
            $landlord = $landlordRepository->findClientByEmail($data[BookingObjectEnum::BOOKING_OBJECT_LANDLORD]);
            if (!$landlord) {
                return new JsonResponse(['error' => 'Client not found'], 404);
            }

            if ($data[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_PENSION) {
                $booking_object = new Pension();
            } elseif ($data[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_COTTAGE) {
                $booking_object = new Cottage();
            }


            $booking_object->__fromArray($data);
            $booking_object->setLandlord($landlord);
            if ($bookingObjectRepository->findObjectByName($booking_object)) {
                return new JsonResponse(
                    ['error' => 'You can not register an object with this name. This name already exists'],
                    412
                );
            }

            $manager->persist($booking_object);
            $manager->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 406);
        }
        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/get/all")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param BookingObjectHandler $bookingObjectHandler
     * @return JsonResponse | NotFoundHttpException
     * @throws NonUniqueResultException
     */
    public function getAllObjectsAction(
        BookingObjectRepository $bookingObjectRepository,
        BookingObjectHandler $bookingObjectHandler
    )
    {
        $objects = $bookingObjectRepository->findAll();
        if (!$objects) {
            throw new NotFoundHttpException();
        }
        return new JsonResponse(['booking_objects' => $bookingObjectHandler->arrayOfEntitiesInArray($objects)]);
    }

    /**
     * @Route("/get/filter")
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @param CottageRepository $cottageRepository
     * @param PensionRepository $pensionRepository
     * @param BookingObjectHandler $bookingObjectHandler
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getBookingObjectsWithFilter(
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        CottageRepository $cottageRepository,
        PensionRepository $pensionRepository,
        BookingObjectHandler $bookingObjectHandler
    )
    {
        $data = $request->query->all();
        if (isset($data[BookingObjectEnum::BOOKING_OBJECT_TYPE])) {
            if ($data[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_PENSION) {
                $objects = $pensionRepository->getObjectsWithFiltering($data);
            } elseif ($data[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_COTTAGE) {
                $objects = $cottageRepository->getObjectsWithFiltering($data);
            }
        } else {
            $objects = $bookingObjectRepository->getObjectsWithFiltering($data);
        }
        return new JsonResponse(['booking_objects' => $bookingObjectHandler->arrayOfEntitiesInArray($objects)]);
    }

    /**
     * @Route("/booking/last", name="get_last_booking_date")
     * @param BookingRepository $bookingRepository
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getLastBookingDate(
        BookingRepository $bookingRepository,
        Request $request,
        BookingObjectRepository $bookingObjectRepository)
    {
        $data = $request->query->all();
        $data = $data[0];

        $bookingObject = $bookingObjectRepository->findObjectByName($data[BookingObjectEnum::BOOKING_OBJECT_NAME]);

        /** @var Booking | null $booking */
        $booking = $bookingRepository
            ->getLastBookingDate($bookingObject->getId(), $data[BookingObjectEnum::BOOKING_OBJECT_ROOM]);

        ($booking == null) ? $date = new DateTime() : $date = $booking->getDateTo();


        return new JsonResponse(['date' => $date]);
    }

    /**
     * @Route("/booking/create")
     * @Method("POST")
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param BookingObjectRepository $bookingObjectRepository
     * @param BookingRepository $bookingRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function createBookingAction(
        Request $request,
        ClientRepository $clientRepository,
        BookingObjectRepository $bookingObjectRepository,
        BookingRepository $bookingRepository,
        EntityManagerInterface $entityManager
    )
    {
        $data = $request->request->all();
        $booking = new Booking();
        /** @var Tenant $tenant */
        $tenant = $clientRepository->findClientByEmail($data['tenant_email']);
        $currentDate = new \DateTime();
        $object = $bookingObjectRepository->findObjectByName($data[BookingObjectEnum::BOOKING_OBJECT_NAME]);
        /** @var Booking | null $tenantLastBooking */
        $tenantLastBooking = $tenant->getBookings()->last();
        if ($tenantLastBooking) {
            if ($tenantLastBooking->getDateFrom() > $currentDate) {
                return new JsonResponse(
                    ['error' => 'You have already booked the property. Wait until the end of the reservation.'],
                    412
                );
            }
        }
        /** @var Booking | null $lastBooking */
        $lastBooking = $bookingRepository->getLastBookingDate($object->getId(), $data[BookingObjectEnum::BOOKING_OBJECT_ROOM]);
        ($lastBooking == null) ? $lastBookingDate = new DateTime() : $lastBookingDate = $lastBooking->getDateTo();

        $booking
            ->setDate(new DateTime())
            ->setDateFrom($lastBookingDate)
            ->setBookingObject($object)
            ->setTenant($tenant)
            ->setRoomNumber($data[BookingObjectEnum::BOOKING_OBJECT_ROOM])
            ->setDateTo($lastBookingDate->modify('+7 day'));

        $entityManager->persist($booking);
        $entityManager->flush();

        return new JsonResponse(['result' => 'ok']);

    }

}