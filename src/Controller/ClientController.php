<?php


namespace App\Controller;

use App\Entity\BookingObject;
use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Enum\ClientEnum;
use App\Repository\TenantRepository;
use App\Repository\UserRepository;
use DateTime;
use Psr\Log\LoggerInterface;
use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ClientController
 * @package App\Controller
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * @Route("/exist/{email}/{passport_id}", name="client_exist")
     * @Method("HEAD")
     * @param string $email
     * @param string $passport_id
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function existClientAction(
        string $email,
        string $passport_id,
        ClientRepository $clientRepository
    )
    {
        if ($clientRepository->findOneByPassportAndEmail($passport_id, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check/{email}/{password}", name="check")
     * @Method("HEAD")
     * @param string $email
     * @param $password
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientAction(
        string $email,
        string $password,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler
    )
    {
        if ($clientRepository->findOneByEmailAndPassword(
            $clientHandler->passwordHashing($password),
            $email)
        ) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/get", name="get_client")
     * @Method("GET")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getClientAction(ClientRepository $clientRepository, Request $request)
    {
        if ($request->query->get('email')) {
            /** @var Client $client */
            $client = $clientRepository->findClientByEmail($request->query->get('email'));
        } elseif ($request->query->get('network') && $request->query->get('uid')) {
            /** @var Client $client */
            $client = $clientRepository->findClientBySocial($request->query->get('network'),
                $request->query->get('uid'));
        }
        return new JsonResponse(
            $client->__toArray()
        );
    }

    /**
     * @Route("/create")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data = $request->request->all();

        if (empty($data['email']) || empty($data['passport_id']) || empty($data['password'])) {
            return new JsonResponse([
                'error' => 'Insufficient data. You have transferred: ' . var_export($data, 1)
            ],
                406);
        }

        if ($clientRepository->findOneByPassportAndEmail($data['passport_id'], $data['email'])) {
            return new JsonResponse(['error' => 'Customer already exists'], 406);
        }

        if (in_array(ClientEnum::ROLE_TENANT, $data[ClientEnum::ROLES])) {
            $client = $clientHandler->createNewTenant($data);
        } elseif (in_array(ClientEnum::ROLE_LANDLORD, $data[ClientEnum::ROLES])) {
            $client = $clientHandler->createNewLandlord($data);
        }

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/change_password")
     * @Method("PUT")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function changePasswordAction(
        ClientRepository $clientRepository,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['password'] = $request->request->get('password');
        if (empty($data['email']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'There is not enough data to change the password. Email:' .
                isset($data['email']) . '. Password: ' . isset($data['password'])], 406);
        }
        $client = $clientRepository->findClientByEmail($data['email']);
        if ($client === null) {
            return new JsonResponse(['error' => 'Client not found'], 404);
        }
        $client->setPassword($data['password']);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/bind_social", name="client_bind_social")
     * @Method("PUT")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $objectManager
     * @param Request $request
     * @return JsonResponse
     */
    public function bindSocialNetworkAction(
        ClientRepository $clientRepository,
        ObjectManager $objectManager,
        Request $request
    )
    {
        $clientEmail = $request->request->get('email');

        /** @var Client $client */
        $client = $clientRepository->findClientByEmail($clientEmail);

        if ($client === null) {
            return new JsonResponse(['error' => 'Client not found'], 404);
        }

        if ($client->bindSocialNetwork([
            'email' => $clientEmail,
            'network' => $request->request->get('network'),
            'uid' => $request->request->get('uid')
        ])) {
            $objectManager->flush();
            return new JsonResponse(['result' => 'ok'], 200);
        } else {
            return new JsonResponse(['error' => 'Error'], 400);
        }
    }

    /**
     * @Route("/check_by_social/{network}/{uid}")
     * @Method("HEAD")
     * @param string $network
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse|NotFoundHttpException
     * @throws NonUniqueResultException
     */
    public function checkClientBySocial(
        string $network,
        string $uid,
        ClientRepository $clientRepository
    )
    {
        if ($clientRepository->findClientBySocial($network, $uid)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            return new NotFoundHttpException();
        }
    }

    /**
     * @Route("/edit")
     * @Method("PUT")
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse | NotFoundHttpException
     */
    public function editClientAction(
        Request $request,
        ClientRepository $clientRepository,
        EntityManagerInterface $entityManager
    )
    {
        /** @var Client $client */
        $client = $clientRepository->findClientByEmail($request->request->get('current_email'));

        if (true) {
            $client->__fromArray($request->request->all());
            $entityManager->flush();
            return new JsonResponse(['result' => 'ok']);
        }
        return new NotFoundHttpException();
    }

    /**
     * @Route("/password/hashing", name="client_password_hashing")
     * @Method("GET")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function hashingPasswordAction(ClientHandler $clientHandler, Request $request)
    {
        return new JsonResponse([
            'result' => $clientHandler->passwordHashing($request->query->get('plainPassword'))
        ]);
    }

    /**
     * @Route("/landlord/object/get_all/{email}", name="get_all_landlord_objects")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @param BookingObjectHandler $bookingObjectHandler
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getLandlordObjects(
        string $email,
        ClientRepository $clientRepository,
        BookingObjectHandler $bookingObjectHandler
    )
    {
        /** @var Landlord $landlord */
        $landlord = $clientRepository->findClientByEmail($email);
        if (!$landlord) {
            return new JsonResponse(['error' => 'Landlord not found'], 406);
        }
        $booking_objects = $bookingObjectHandler->arrayOfEntitiesInArray($landlord->getBookingObjects());
        return new JsonResponse(['booking_objects' => $booking_objects]);
    }

}