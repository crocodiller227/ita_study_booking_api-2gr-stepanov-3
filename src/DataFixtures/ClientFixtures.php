<?php

namespace App\DataFixtures;


use App\Model\Client\ClientHandler;
use App\Model\Enum\ClientEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $landlord = $this->clientHandler->createNewLandlord([
            ClientEnum::EMAIL => 'landlord@bookong.root',
            ClientEnum::PASSPORT_ID => 'LANDLORD666',
            ClientEnum::USERNAME => 'landlord',
            ClientEnum::ENABLED => true,
            ClientEnum::PASSWORD => 'root',
            ClientEnum::FULL_NAME => 'Mr. Landlord'
        ]);
        $manager->persist($landlord);
        $this->addReference('landlord', $landlord);


        $tenant = $this->clientHandler->createNewTenant([
            ClientEnum::EMAIL => 'tenant@bookong.root',
            ClientEnum::PASSPORT_ID => 'TENANT666',
            ClientEnum::USERNAME => 'tenant',
            ClientEnum::ENABLED => true,
            ClientEnum::PASSWORD => 'root',
            ClientEnum::FULL_NAME => 'Mr. Tenant'
        ]);
        $manager->persist($tenant);

        $manager->flush();
    }
}