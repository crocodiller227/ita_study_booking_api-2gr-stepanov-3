<?php


namespace App\DataFixtures;


use App\Entity\Landlord;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Enum\BookingObjectEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CottageFixtures extends Fixture implements DependentFixtureInterface
{

    private $bookingObjectHandler;


    public function __construct(BookingObjectHandler $bookingObjectHandler)
    {
        $this->bookingObjectHandler = $bookingObjectHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $cottages = [
            BookingObjectEnum::BOOKING_OBJECT_NAME => ['Hilton', 'Hot River', 'Salt Sea'],
            BookingObjectEnum::BOOKING_OBJECT_ADDRESS => [
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
            ],
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON => ['John Doe', 'Vasya Pupkin', 'Rahman uulu Kalybek'],
            BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS => [4, 20, 2],
            BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT => [60.90, 45.14, 88.17],
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE => ['+996666666666', '+996999999999', '+996111111111'],
            BookingObjectEnum::BOOKING_COTTAGE_SAUNA => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_COTTAGE_KITTEN => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_COTTAGE_BATHROOM => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_COTTAGE_BILLIARDS => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_COTTAGE_SWIMMING_POOL => [rand(0, 1), rand(0, 1), rand(0, 1)]
        ];

        /** @var Landlord $landlord */
        $landlord = $this->getReference('landlord');

        foreach ($cottages[BookingObjectEnum::BOOKING_OBJECT_NAME] as $cottageKey => $cottageOption) {
            $cottageData = [];
            foreach ($cottages as $optionsName => $options) {
                $cottageData[$optionsName] = $options[$cottageKey];
            }
            $cottage = $this->bookingObjectHandler->createNewCottage($cottageData);
            $cottage->setLandlord($landlord);
            $manager->persist($cottage);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ClientFixtures::class,
        );
    }
}