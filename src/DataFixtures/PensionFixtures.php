<?php

namespace App\DataFixtures;

use App\Entity\Landlord;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Enum\BookingObjectEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class PensionFixtures extends Fixture implements DependentFixtureInterface
{

    private $bookingObjectHandler;


    public function __construct(BookingObjectHandler $bookingObjectHandler)
    {
        $this->bookingObjectHandler = $bookingObjectHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(
        ObjectManager $manager
    )
    {
        $pensions = [
            BookingObjectEnum::BOOKING_OBJECT_NAME => ['SunDay', 'Silk Road', 'Camel'],
            BookingObjectEnum::BOOKING_OBJECT_ADDRESS => [
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
                $this->bookingObjectHandler->getRandomCoordinatesUrl(),
            ],
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON => ['Luke Cage', 'Ivan Ivanov', 'Manasbekov Semetey'],
            BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS => [4, 20, 2],
            BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT => [60.90, 45.14, 88.17],
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE => ['+996666666555', '+996999999444', '+996111111333'],
            BookingObjectEnum::BOOKING_PENSION_TELEVISION_SET => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_PENSION_THREE_MEALS_A_DAY => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_PENSION_AIR_CONDITIONING => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_PENSION_CLEANING_OF_ROOMS => [rand(0, 1), rand(0, 1), rand(0, 1)],
            BookingObjectEnum::BOOKING_PENSION_SHOWER_IN_THE_ROOM => [rand(0, 1), rand(0, 1), rand(0, 1)],
        ];

        /** @var Landlord $landlord */
        $landlord = $this->getReference('landlord');

        foreach ($pensions[BookingObjectEnum::BOOKING_OBJECT_NAME] as $pensionKey => $pensionOption) {
            $pensionData = [];
            foreach ($pensions as $optionsName => $options) {
                $pensionData[$optionsName] = $options[$pensionKey];
            }
            $pension = $this->bookingObjectHandler->createNewPension($pensionData);
            $pension->setLandlord($landlord);
            $manager->persist($pension);
        }

        $manager->flush();
    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ClientFixtures::class,
        );
    }
}