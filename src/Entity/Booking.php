<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $dateFrom;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $dateTo;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $bookingPeriodDays;

    /**
     * @var Tenant
     * @ManyToOne(targetEntity="Tenant", inversedBy="bookings")
     * @JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    private $tenant;

    /**
     * @ManyToOne(targetEntity="BookingObject", inversedBy="bookings")
     * @JoinColumn(name="booking_object_id", referencedColumnName="id")
     */
    private $bookingObject;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $roomNumber;

    public function __construct()
    {
        $this->bookingPeriodDays = 7;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $date
     * @return Booking
     */
    public function setDate(\DateTime $date): Booking
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $dateFrom
     * @return Booking
     */
    public function setDateFrom(DateTime $dateFrom): Booking
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTime $dateTo
     * @return Booking
     */
    public function setDateTo(DateTime $dateTo): Booking
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        return $this->dateTo;
    }

    /**
     * @param int $bookingPeriodDays
     * @return Booking
     */
    public function setBookingPeriodDays(int $bookingPeriodDays): Booking
    {
        $this->bookingPeriodDays = $bookingPeriodDays;
        return $this;
    }

    /**
     * @return int
     */
    public function getBookingPeriodDays(): int
    {
        return $this->bookingPeriodDays;
    }

    /**
     * @param Tenant $tenant
     * @return Booking
     */
    public function setTenant(Tenant $tenant): Booking
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Tenant
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @param mixed $bookingObject
     * @return Booking
     */
    public function setBookingObject($bookingObject)
    {
        $this->bookingObject = $bookingObject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingObject()
    {
        return $this->bookingObject;
    }

    /**
     * @param int $roomNumber
     * @return Booking
     */
    public function setRoomNumber(int $roomNumber): Booking
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getRoomNumber(): int
    {
        return $this->roomNumber;
    }


}
