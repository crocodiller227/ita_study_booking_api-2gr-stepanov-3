<?php

namespace App\Entity;

use App\Model\Enum\BookingObjectEnum;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"BookingObject" = "BookingObject", "Pension" = "Pension", "Cottage" = "Cottage"})
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 */
class BookingObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactPerson;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactPersonPhone;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $numberOfRooms;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $pricePerNight;

    /**
     * @var Landlord
     * @ManyToOne(targetEntity="Landlord", inversedBy="bookingObjects")
     * @JoinColumn(name="landlord_id", referencedColumnName="id")
     */
    private $landlord;

    /**
     * @OneToMany(targetEntity="Booking", mappedBy="bookingObject")
     */
    private $bookings;

    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            BookingObjectEnum::BOOKING_OBJECT_NAME => $this->name,
            BookingObjectEnum::BOOKING_OBJECT_ADDRESS => $this->address,
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON => $this->contactPerson,
            BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS => $this->numberOfRooms,
            BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT => $this->pricePerNight,
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE => $this->contactPersonPhone,
            BookingObjectEnum::BOOKING_OBJECT_LANDLORD => $this->landlord->getEmail(),
        ];
    }

    /**
     * @param array $bookingObjectData
     * @return $this
     */
    public function __fromArray(array $bookingObjectData)
    {
        $this->name = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_NAME];
        $this->address = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_ADDRESS];
        $this->contactPerson = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON];
        $this->numberOfRooms = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS];
        $this->pricePerNight = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT];
        $this->contactPersonPhone = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE];

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getContactPerson(): ?string
    {
        return $this->contactPerson;
    }

    public function setContactPerson(string $contactPerson): self
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    public function getContactPersonPhone(): ?string
    {
        return $this->contactPersonPhone;
    }

    public function setContactPersonPhone(string $contactPersonPhone): self
    {
        $this->contactPersonPhone = $contactPersonPhone;

        return $this;
    }

    public function getPricePerNight()
    {
        return $this->pricePerNight;
    }

    public function setPricePerNight($pricePerNight): self
    {
        $this->pricePerNight = $pricePerNight;

        return $this;
    }

    /**
     * @param int $numberOfRooms
     * @return BookingObject
     */
    public function setNumberOfRooms(int $numberOfRooms): BookingObject
    {
        $this->numberOfRooms = $numberOfRooms;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfRooms(): int
    {
        return $this->numberOfRooms;
    }

    /**
     * @param Landlord $landlord
     * @return BookingObject
     */
    public function setLandlord(Landlord $landlord): BookingObject
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return Landlord
     */
    public function getLandlord(): Landlord
    {
        return $this->landlord;
    }

    /**
     * @param mixed $bookings
     * @return BookingObject
     */
    public function setBookings($bookings)
    {
        $this->bookings = $bookings;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }
}
