<?php

namespace App\Entity;

use App\Model\Enum\ClientEnum;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"Client" = "Client", "Tenant" = "Tenant", "Landlord" = "Landlord"})
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $full_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $roles;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     * @ORM\Column(name="passport_id", type="string", length=255, unique=true)
     */
    protected $passportId;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialFacebook;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialVkontakte;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialGoogle;

    public function __construct()
    {
        $this->enabled = false;
        $this->roles = json_encode([ClientEnum::ROLE_USER]);
    }


    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            ClientEnum::EMAIL => $this->email,
            ClientEnum::ENABLED => $this->enabled,
            ClientEnum::PASSWORD => $this->password,
            ClientEnum::USERNAME => $this->username,
            ClientEnum::FULL_NAME => $this->full_name,
            ClientEnum::PASSPORT_ID => $this->passportId,
            ClientEnum::GOOGLE => $this->socialGoogle,
            ClientEnum::FACEBOOK => $this->socialFacebook,
            ClientEnum::VKONTAKTE => $this->socialVkontakte,
            ClientEnum::ROLES => json_decode($this->roles, true),
        ];
    }

    /**
     * @param array $userData
     * @return $this
     */
    public function __fromArray(array $userData)
    {
        $this->email = $userData[ClientEnum::EMAIL];
        $this->username = $userData[ClientEnum::USERNAME];
        $this->password = $userData[ClientEnum::PASSWORD];
        $this->passportId = $userData[ClientEnum::PASSPORT_ID];
        $this->enabled = $userData[ClientEnum::ENABLED] ?? false;
        $this->full_name = $userData[ClientEnum::FULL_NAME] ?? null;
        $this->socialGoogle = $userData[ClientEnum::GOOGLE] ?? null;
        $this->socialFacebook = $userData[ClientEnum::FACEBOOK] ?? null;
        $this->socialVkontakte = $userData[ClientEnum::VKONTAKTE] ?? null;

        return $this;
    }


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Client
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }


    /**
     * @return null|string
     */
    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    /**
     * @param null | string $full_name
     * @return Client
     */
    public function setFullName(?string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Client
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Client
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return array The client roles
     */
    public function getRoles(): array
    {
        return json_decode($this->roles, true);
    }

    /**
     * @param array $roles
     * @return Client
     */
    public function setRoles($roles)
    {
        $this->roles = json_encode($roles);
        return $this;
    }

    /**
     * @param string $roles
     * @return Client
     */
    public function addRoles($roles)
    {
        $decodedRoles = json_decode($roles, true);
        if (gettype($decodedRoles) == 'array') {
            $this->roles = json_encode(
                array_values(
                    array_filter(
                        array_unique(
                            array_merge(
                                json_decode($this->roles, true) ?? [],
                                $decodedRoles
                            )
                        )
                    )
                )
            );
        } else {
            $current_roles = json_decode($this->roles, true) ?? [];
            $current_roles[] = $roles;
            $this->roles = json_encode($current_roles);
        }
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Client
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }


    /**
     * @param string $passportId
     * @return Client
     */
    public function setPassportId(string $passportId): Client
    {
        $this->passportId = $passportId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassportId(): ?string
    {
        return $this->passportId;
    }

    /**
     * @param null | string $socialFacebook
     * @return Client
     */
    public function setSocialFacebook(?string $socialFacebook): Client
    {
        $this->socialFacebook = $socialFacebook;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialFacebook(): string
    {
        return $this->socialFacebook;
    }

    /**
     * @param string | null $socialVkontakte
     * @return Client
     */
    public function setSocialVkontakte(?string $socialVkontakte): Client
    {
        $this->socialVkontakte = $socialVkontakte;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialVkontakte(): string
    {
        return $this->socialVkontakte;
    }

    /**
     * @param string | null $socialGoogle
     * @return Client
     */
    public function setSocialGoogle(?string $socialGoogle): Client
    {
        $this->socialGoogle = $socialGoogle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialGoogle(): string
    {
        return $this->socialGoogle;
    }

    /**
     * @param array $socialData
     * @return Client | boolean
     */
    public function bindSocialNetwork(array $socialData): self
    {
        switch ($socialData['network']) {
            case 'facebook':
                $this->socialFacebook = $socialData['uid'];
                break;
            case 'vkontakte' :
                $this->socialVkontakte = $socialData['uid'];
                break;
            case 'google':
                $this->socialGoogle = $socialData['uid'];
                break;
            default:
                return false;
                break;
        }
        return $this;
    }
}
