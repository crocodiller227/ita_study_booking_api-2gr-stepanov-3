<?php

namespace App\Entity;

use App\Model\Enum\ClientEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LandlordRepository")
 */
class Landlord extends Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="BookingObject", mappedBy="landlord")
     */
    private $bookingObjects;

    public function __construct()
    {
        parent::__construct();
        $this->addRoles(ClientEnum::ROLE_LANDLORD);
        $this->bookingObjects = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param ArrayCollection $bookingObjects
     * @return Landlord
     */
    public function setBookingObjects(ArrayCollection $bookingObjects): Landlord
    {
        $this->bookingObjects = $bookingObjects;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingObjects()
    {
        return $this->bookingObjects;
    }


}
