<?php

namespace App\Entity;

use App\Model\Enum\BookingObjectEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $televisionSet;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $showerInTheRoom;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $airConditioning;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $cleaningOfRooms;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $threeMealsADay;

    /**
     * @return array
     */
    public function __toArray()
    {
        return array_merge(parent::__toArray(), [
            BookingObjectEnum::BOOKING_PENSION_TELEVISION_SET => $this->televisionSet,
            BookingObjectEnum::BOOKING_PENSION_THREE_MEALS_A_DAY => $this->threeMealsADay,
            BookingObjectEnum::BOOKING_PENSION_AIR_CONDITIONING => $this->airConditioning,
            BookingObjectEnum::BOOKING_PENSION_CLEANING_OF_ROOMS => $this->cleaningOfRooms,
            BookingObjectEnum::BOOKING_PENSION_SHOWER_IN_THE_ROOM => $this->showerInTheRoom,
            BookingObjectEnum::BOOKING_OBJECT_TYPE => BookingObjectEnum::BOOKING_OBJECT_PENSION,
        ]);
    }

    /**
     * @param array $bookingObjectData
     * @return $this
     */
    public function __fromArray(array $bookingObjectData)
    {
        parent::__fromArray($bookingObjectData);

        $this->televisionSet = $bookingObjectData[BookingObjectEnum::BOOKING_PENSION_TELEVISION_SET];
        $this->threeMealsADay = $bookingObjectData[BookingObjectEnum::BOOKING_PENSION_THREE_MEALS_A_DAY];
        $this->airConditioning = $bookingObjectData[BookingObjectEnum::BOOKING_PENSION_AIR_CONDITIONING];
        $this->cleaningOfRooms = $bookingObjectData[BookingObjectEnum::BOOKING_PENSION_CLEANING_OF_ROOMS];
        $this->showerInTheRoom = $bookingObjectData[BookingObjectEnum::BOOKING_PENSION_SHOWER_IN_THE_ROOM];

        return $this;
    }


    /**
     * @param bool $televisionSet
     * @return Pension
     */
    public function setTelevisionSet(bool $televisionSet): Pension
    {
        $this->televisionSet = $televisionSet;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTelevisionSet(): bool
    {
        return $this->televisionSet;
    }

    /**
     * @param bool $showerInTheRoom
     * @return Pension
     */
    public function setShowerInTheRoom(bool $showerInTheRoom): Pension
    {
        $this->showerInTheRoom = $showerInTheRoom;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowerInTheRoom(): bool
    {
        return $this->showerInTheRoom;
    }

    /**
     * @param bool $airConditioning
     * @return Pension
     */
    public function setAirConditioning(bool $airConditioning): Pension
    {
        $this->airConditioning = $airConditioning;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAirConditioning(): bool
    {
        return $this->airConditioning;
    }

    /**
     * @param bool $cleaningOfRooms
     * @return Pension
     */
    public function setCleaningOfRooms(bool $cleaningOfRooms): Pension
    {
        $this->cleaningOfRooms = $cleaningOfRooms;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCleaningOfRooms(): bool
    {
        return $this->cleaningOfRooms;
    }

    /**
     * @param bool $threeMealsADay
     * @return Pension
     */
    public function setThreeMealsADay(bool $threeMealsADay): Pension
    {
        $this->threeMealsADay = $threeMealsADay;
        return $this;
    }

    /**
     * @return bool
     */
    public function isThreeMealsADay(): bool
    {
        return $this->threeMealsADay;
    }

}
