<?php

namespace App\Entity;

use App\Model\Enum\ClientEnum;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="tenant")
     */
    private $bookings;

    public function __construct()
    {
        parent::__construct();
        $this->addRoles(ClientEnum::ROLE_TENANT);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    public function setBooking($bookings)
    {
        $this->bookings = $bookings;
        return $this;
    }

    public function getBookings()
    {
        return $this->bookings;
    }

}
