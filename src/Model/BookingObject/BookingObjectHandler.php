<?php


namespace App\Model\BookingObject;


use App\Entity\Booking;
use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Model\Enum\BookingObjectEnum;
use App\Repository\BookingRepository;
use DateTime;

class BookingObjectHandler
{
    private $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * @param array $data
     * @return Pension
     */
    public function createNewPension(array $data)
    {
        $pension = new Pension();
        $pension->__fromArray($data);
        return $pension;
    }

    public function createNewCottage(array $data)
    {
        $cottage = new Cottage();
        $cottage->__fromArray($data);
        return $cottage;
    }

    /**
     * @param BookingObject[] $arrayOfEntities
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function arrayOfEntitiesInArray($arrayOfEntities)
    {
        $booking_objects = [];
        foreach ($arrayOfEntities as $booking_object) {
            for ($i = $booking_object->getNumberOfRooms(); $i > 0; $i--) {
                $booking_object = clone $booking_object;
                $objectInArray = $booking_object->__toArray();

                /** @var Booking $lastBooking */
                $lastBooking = $this->bookingRepository->getLastBookingDate($booking_object->getId(), $i);
                $objectInArray[BookingObjectEnum::BOOKING_OBJECT_LAST_BOOKING_DATE_FROM] =
                    ($lastBooking) ? $lastBooking->getDateTo() : new DateTime();
                $objectInArray[BookingObjectEnum::BOOKING_OBJECT_ROOM] = $i;
                array_push($booking_objects, $objectInArray);
            }
        }

        return $booking_objects;
    }

    public function getRandomCoordinatesUrl()
    {
        return 'https://www.google.com/maps/place/'
            . rand(41, 43) . '.' . rand(100000, 999999)
            . ',' . rand(75, 78) . '.' . rand(100000, 999999);
    }
}