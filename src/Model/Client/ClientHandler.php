<?php

namespace App\Model\Client;

use App\Entity\Landlord;
use App\Entity\Tenant;

class ClientHandler
{
    /**
     * @param Landlord | Tenant $client
     * @param array $data
     * @param bool $encodingPassword
     * @return Landlord | Tenant
     */
    public function createNewAbstractClient($client, array $data, bool $encodingPassword = true)
    {
        $client->__fromArray($data);

        if ($encodingPassword) {
            $password = $this->passwordHashing($data['password']);
        } else {
            $password = $data['password'];
        }
        $client->setPassword($password);

        return $client;
    }

    /**
     * @param array $data
     * @param bool $encodingPassword
     * @return Landlord
     */
    public function createNewLandlord(array $data, bool $encodingPassword = true)
    {
        return $this->createNewAbstractClient(new Landlord(), $data, $encodingPassword);
    }

    /**
     * @param array $data
     * @param bool $encodingPassword
     * @return Tenant
     */
    public function createNewTenant(array $data, bool $encodingPassword = true)
    {
        return $this->createNewAbstractClient(new Tenant(), $data, $encodingPassword);
    }

    /**
     * @param string $plainPassword
     * @return bool|string
     */
    public function passwordHashing(string $plainPassword)
    {
        return password_hash($plainPassword,
            PASSWORD_BCRYPT,
            ['cost' => 12, 'salt' => md5('lolkekcheburek')]);
    }
}