<?php

namespace App\Repository;

use App\Entity\BookingObject;
use App\Model\Enum\BookingObjectEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function getObjectsWithFiltering(array $data)
    {
        $db = $this
            ->createQueryBuilder('o')
            ->select('o')
            ->where('o.pricePerNight BETWEEN :price_min AND :price_max')
            ->setParameter('price_min', $data['price_min'] ?? 0)
            ->setParameter('price_max', $data['price_max'] ?? 999999999999999);

        if (isset($data['filter_search'])) {
            $db
                ->andWhere('o.name like :name')
                ->setParameter('name', '%' . $data['filter_search'] . '%');
        }

        return $db->getQuery()->getResult();
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findObjectByName($name)
    {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->where('o.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
