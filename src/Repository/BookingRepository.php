<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param $booking_object_id
     * @param $booking_object_room
     * @return \Doctrine\ORM\Query\Parameter|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastBookingDate($booking_object_id, $booking_object_room)
    {
        return $this
            ->createQueryBuilder('b')
            ->select('b')
            ->where('b.bookingObject = :booking_object')
            ->andWhere('b.roomNumber = :room_number')
            ->setParameter('booking_object', $booking_object_id)
            ->setParameter('room_number', $booking_object_room)
            ->orderBy('b.dateTo', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('b')
            ->where('b.something = :value')->setParameter('value', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
