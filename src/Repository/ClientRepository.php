<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @param string $passportID
     * @param string $email
     * @return Client|null
     */
    public function findOneByPassportAndEmail(string $passportID, string $email): ?Client
    {
        try {
            return $this
                ->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->orWhere('a.passportId = :passport_id')
                ->setParameter('email', $email)
                ->setParameter('passport_id', $passportID)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmailAndPassword(string $password, string $email): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->orWhere('a.email = :email')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findClientByEmail($email)
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $network
     * @param $uid
     * @return Client | null
     * @throws NonUniqueResultException
     */
    public function findClientBySocial(string $network, string $uid)
    {
        switch ($network) {
            case 'facebook':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialFacebook = :uid')
                    ->setParameter('uid', $uid)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            case 'vkontakte':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialVkontakte = :uid')
                    ->setParameter('uid', $uid)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            case 'google':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialGoogle = :uid')
                    ->setParameter('uid', $uid)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            default:
                return null;
        }
        return $user;
    }
}
