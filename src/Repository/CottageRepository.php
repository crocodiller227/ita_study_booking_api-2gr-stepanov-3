<?php

namespace App\Repository;

use App\Entity\Cottage;
use App\Model\Enum\BookingObjectEnum;

class CottageRepository extends BookingObjectRepository
{
    public function getObjectsWithFiltering(array $data)
    {
        $db = $this
            ->createQueryBuilder('o')
            ->select('o')
            ->innerJoin(Cottage::class, 'c', 'WITH', 'c.id = o.id')
            ->where('o.pricePerNight BETWEEN :price_min AND :price_max')
            ->setParameter('price_min', $data['price_min'] ?? 0)
            ->setParameter('price_max', $data['price_max'] ?? 999999999999999);

        if (isset($data['filter_search'])) {
            $db
                ->andWhere('o.name like :name')
                ->setParameter('name', '%' . $data['filter_search'] . '%');
        }
        if (isset($data[BookingObjectEnum::BOOKING_COTTAGE_SAUNA])) {
            $db
                ->andWhere('c.sauna = :sauna')
                ->setParameter('sauna', $data[BookingObjectEnum::BOOKING_COTTAGE_SAUNA]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_COTTAGE_BILLIARDS])) {
            $db
                ->andWhere('c.billiards = :billiards')
                ->setParameter('billiards', $data[BookingObjectEnum::BOOKING_COTTAGE_BILLIARDS]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_COTTAGE_SWIMMING_POOL])) {
            $db
                ->andWhere('c.swimmingPool = :swimming_pool')
                ->setParameter('swimming_pool', $data[BookingObjectEnum::BOOKING_COTTAGE_SWIMMING_POOL]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_COTTAGE_BATHROOM])) {
            $db
                ->andWhere('c.bathroom = :bathroom')
                ->setParameter('bathroom', $data[BookingObjectEnum::BOOKING_COTTAGE_BATHROOM]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_COTTAGE_KITTEN])) {
            $db
                ->andWhere('c.kitten = :kitten')
                ->setParameter('kitten', $data[BookingObjectEnum::BOOKING_COTTAGE_KITTEN]);
        }
        return $db->getQuery()->getResult();
    }
}
