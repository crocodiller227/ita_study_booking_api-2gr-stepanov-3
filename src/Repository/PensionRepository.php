<?php

namespace App\Repository;

use App\Entity\Pension;
use App\Model\Enum\BookingObjectEnum;

class PensionRepository extends BookingObjectRepository
{
    public function getObjectsWithFiltering(array $data)
    {
        $db = $this
            ->createQueryBuilder('o')
            ->select('o')
            ->innerJoin(Pension::class, 'p', 'WITH', 'p.id = o.id')
            ->where('o.pricePerNight BETWEEN :price_min AND :price_max')
            ->setParameter('price_min', $data['price_min'] ?? 0)
            ->setParameter('price_max', $data['price_max'] ?? 999999999999999);

        if (isset($data['filter_search'])) {
            $db
                ->andWhere('o.name like :name')
                ->setParameter('name', '%' . $data['filter_search'] . '%');
        }
        if (isset($data[BookingObjectEnum::BOOKING_PENSION_THREE_MEALS_A_DAY])) {
            $db
                ->andWhere('p.threeMealsADay = :three_meals_a_day')
                ->setParameter('three_meals_a_day', $data[BookingObjectEnum::BOOKING_PENSION_THREE_MEALS_A_DAY]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_PENSION_CLEANING_OF_ROOMS])) {
            $db
                ->andWhere('p.cleaningOfRooms = :cleaning_of_rooms')
                ->setParameter('cleaning_of_rooms', $data[BookingObjectEnum::BOOKING_PENSION_CLEANING_OF_ROOMS]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_PENSION_AIR_CONDITIONING])) {
            $db
                ->andWhere('p.airConditioning = :air_conditioning')
                ->setParameter('air_conditioning', $data[BookingObjectEnum::BOOKING_PENSION_AIR_CONDITIONING]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_PENSION_SHOWER_IN_THE_ROOM])) {
            $db
                ->andWhere('p.showerInTheRoom = :shower_in_the_room')
                ->setParameter('shower_in_the_room', $data[BookingObjectEnum::BOOKING_PENSION_SHOWER_IN_THE_ROOM]);
        }
        if (isset($data[BookingObjectEnum::BOOKING_PENSION_TELEVISION_SET])) {
            $db
                ->andWhere('p.televisionSet = :television_set')
                ->setParameter('television_set', $data[BookingObjectEnum::BOOKING_PENSION_TELEVISION_SET]);
        }
        return $db->getQuery()->getResult();
    }
}
